 /*
 
 ---   You only need one opening and closing "$(document).ready(function() {""    ---
 
 $(document).ready(function() {
    $('select').material_select();
     $(".button-collapse").sideNav();
  });

  $(document).ready(function(){
      $('.slider').slider({
        height: 1000
      });
    });
    
    $(document).ready(function(){
      $('.carousel.carousel-slider').carousel({fullWidth: true});
    });
*/

$(document).ready(function() {
    $('select').material_select();
    $(".button-collapse").sideNav();
    $('ul.tabs').tabs();
    //$('.slider').slider({
    //    height: 1000
    //});
    $('.slider').slider();
    $('.carousel.carousel-slider').carousel({fullWidth: true});
    $('.materialboxed').materialbox();
  });
    
