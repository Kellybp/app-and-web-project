<?php include '../views/viewheader.php'; ?>

<br/>
<div class="card">
    <div class="card-content">
        <div class="container">
             <div class="row">
                 <h3 class="center-align">Thank you for signing up please login below</h3>
             </div>
             <div class="row">
                 <a href="login.php" class="btn">Login</a>
             </div>
        </div>
    </div>
</div>
<?php include '../views/viewfooter.php'; ?>