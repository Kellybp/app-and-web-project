<?php include '../views/viewheader.php'; ?>

<h3 class="center">User Manager</h3>
<h4>Current Users Online</h5>
<div class="row">
    <div class="col s12 l12">
        <h4 class="center">Drivers Online</h4>
        <table class="responsive-table striped bordered">
            <thead>
                <tr>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Availability</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>SpongeBob Squarepants</td>
                    <td>lolol@dankmeme.com</td>
                    <td>Offline</td>
                    <td>
                        <select>
                          <option value="" disabled selected>Choose your option</option>
                          <option value="1">Kick Off</option>
                          <option value="2">Send Message</option>
                          <option value="3">Prevent Deliveries</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Patrick Star</td>
                    <td>rock@bikinibottom.com</td>
                    <td>Online</td>
                    <td><select>
                          <option value="" disabled selected>Choose your option</option>
                          <option value="1">Kick Off</option>
                          <option value="2">Send Message</option>
                          <option value="3">Prevent Deliveries</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Patrick Star</td>
                    <td>rock@bikinibottom.com</td>
                    <td>On Delivery</td>
                    <td><select>
                          <option value="" disabled selected>Choose your option</option>
                          <option value="1">Kick Off</option>
                          <option value="2">Send Message</option>
                          <option value="3">Prevent Deliveries</option>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col s12 l12">
        <h4 class="center">Customers Online</h4>
        <table class="responsive-table striped bordered">
            <thead>
                <tr>
                    <th>Username</th>
                    <th>Email Address</th>
                    <th>User Actions</th>
                    <th class="center">Verified?</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>goofbag123</td>
                    <td>gwbush@whitehouse.com</td>
                    <td>
                        <select>
                          <option value="" disabled selected>Choose your option</option>
                          <option value="1">Kick Off</option>
                          <option value="2">Send Message</option>
                          <option value="3">Prevent Purchasing</option>
                        </select>
                    </td>
                    <td class="center"><i class="material-icons">check</i></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col s12 l12">
        <h4 class="center">Unverified Users</h4>
        <table class="responsive-table striped bordered">
            <thead>
                <tr>
                    <th>Username</th>
                    <th>Email</th>
                    <th>View License</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Squidward</td>
                    <td>clarinet@player.com</td>
                    <td class="center"><img class="materialboxed center-align" id="cartImg" data-caption="Name of the Product Goes Here" width="100" src="../images/bean.jpg"></td>
                    <td>
                        <button class="btn waves-effect">Verify</button>
                        <button class="btn waves-effect">Decline</button>
                    </td>
                </tr>
                <tr>
                    <td>Squidward</td>
                    <td>clarinet@player.com</td>
                    <td class="center"><img class="materialboxed center-align" id="cartImg" data-caption="Name of the Product Goes Here" width="100" src="../images/bean.jpg"></td>
                    <td>
                        <button class="btn waves-effect">Verify</button>
                        <button class="btn waves-effect">Decline</button>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<br>
    <br>
<h4>Search For a User By:</h5>
        <div class="row">
            <form class="col s12">
                <div class="row">
                    <div class="input-field col s5">
                      <input placeholder="User Name" id="user_name" type="text" class="validate">
                      <label for="user_name">Username</label>
                    </div>
                    <span class="col s2 center">Or</span>
                    <div class="input-field col s5">
                      <input placeholder="Email Address" id="email" type="text" class="validate">
                      <label for="email">Email Address</label>
                    </div>
                </div>
                <button class="btn waves-effect">Search:</button>
            </form>
        </div>
    <table class="responsive-table striped bordered">
        <thead>
            <tr>
                <th>Username</th>
                <th>Email Address</th>
                <th>Role</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>SpongeBob Squarepants</td>
                <td>lolol@dankmeme.com</td>
                <td>Driver</td>
                    <td>
                        <select>
                          <option value="" disabled selected>Choose your option</option>
                          <option value="1">Kick Off</option>
                          <option value="2">Send Message</option>
                          <option value="3">Prevent Deliveries</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>goofbag123</td>
                    <td>gwbush@whitehouse.com</td>
                    <td>Customer</td>
                    <td>
                        <select>
                          <option value="" disabled selected>Choose your option</option>
                          <option value="1">Kick Off</option>
                          <option value="2">Send Message</option>
                          <option value="3">Prevent Purchasing</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Patrick Star</td>
                    <td>rock@bikinibottom.com</td>
                    <td>Driver</td>
                    <td><select>
                          <option value="" disabled selected>Choose your option</option>
                          <option value="1">Kick Off</option>
                          <option value="2">Send Message</option>
                          <option value="3">Prevent Deliveries</option>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
        <br>
        <br>

<?php include '../views/viewfooter.php'; ?>