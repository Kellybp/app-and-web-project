<?php include '../views/viewheader.php'; ?>

<br/>
<div class="card">
    <div class="card-content">
        <div class="container">
        <form class="col s12" id="register_user_form" action="index.php" method="post">
          <h3 class="center">Register For an Account</h3>
          <input type="hidden" name="action" value="register_user"/>
          <div class="row">
            <div class="input-field col s12">
              <input placeholder="Something Clever" id="userName" type="text" name="userName" class="validate">
              <label for="userName">User Name</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input placeholder="John" id="fName" type="text" name="fName" class="validate">
              <label for="fName">First Name</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input placeholder="Smith" id="lName" type="text" name="lName" class="validate">
              <label for="lName">Last Name</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <input placeholder="Joe@example.com" id="email" type="text" name="email" class="validate">
              <label for="email">Email Address</label>
            </div>
          </div>
          <div class="row">
            <label for="">Marijuana Approval Verification</label>
              <div class="file-field input-field">
              <div class="btn">
              <span>File</span>
              <input type="file">
              </div>
              <div class="file-path-wrapper">
              <input class="file-path validate" type="text">
              </div>
              </div>
              
          </div>
          <div class="row">
              <div class="input-field col s12">
              <input placeholder="Password" id="password" type="text" name="password" class="validate">
              <label for="password">Password</label>
            </div>
          </div>
          <div class="row">
              <div class="input-field col s12">
              <input placeholder="Confirm Password" id="cpassword" type="text" name="cpassword" class="validate">
              <label for="cpassword">Confirm Your Password</label>
            </div>
          </div>
          <div class="center-align">
          <button class="btn waves-effect" type="submit" name="Register">Register</button>
          </div>
        </form>
        </div>
    </div>
</div>
<?php include '../views/viewfooter.php'; ?>