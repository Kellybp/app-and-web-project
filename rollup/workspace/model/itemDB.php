<?php

    function get_items(){
        global $db;
        $stmt = $db ->prepare('SELECT * FROM product');
        $stmt-> execute();
        return $stmt;
    }
    
     function get_items_buds(){
        global $db;
        $stmt = $db ->prepare('SELECT * FROM product WHERE itemCat = "Buds"');
        $stmt-> execute();
        return $stmt;
    }
    
    function get_items_edibles(){
        global $db;
        $stmt = $db ->prepare('SELECT * FROM product WHERE itemCat = "Edibles"');
        $stmt-> execute();
        return $stmt;
    }
    
    function get_items_oils(){
        global $db;
        $stmt = $db ->prepare('SELECT * FROM product WHERE itemCat = "Oils"');
        $stmt-> execute();
        return $stmt;
    }
    
    function get_item_by_ID($itemID){
        global $db;
        $stmt = $db ->prepare('SELECT * FROM product WHERE itemID = :itemID');
        $stmt->bindParam(':itemID', $itemID);
        $stmt-> execute();
        return $stmt;
    }
    
    function add_items($itemName, $itemType, $itemPrice, $itemCat, $itemQty, $itemDesc){
        global $db;
        $stmt = $db->prepare('INSERT INTO product(itemName, itemType, itemPrice, itemCat, itemQty, itemDesc) VALUES (:itemName, :itemType, :itemPrice, :itemCat, :itemQty, :itemDesc)');
        $stmt->bindParam(':itemName', $itemName);
        $stmt->bindParam(':itemType', $itemType);
        $stmt->bindParam(':itemPrice', $itemPrice);
        $stmt->bindParam(':itemCat', $itemCat);
        $stmt->bindParam(':itemQty', $itemQty);
        $stmt->bindParam(':itemDesc', $itemDesc);
        $stmt->execute();
    }
    
    function update_items($itemName, $itemType, $itemPrice, $itemCat, $itemQty, $itemDesc, $itemID){
        global $db;
        $stmt = $db->prepare('UPDATE product SET itemName = :itemName, itemType = :itemType, itemPrice = :itemPrice, itemCat = :itemCat, itemQty = :itemQty, itemDesc = :itemDesc WHERE itemID = :itemID');
        $stmt->bindParam(':itemName', $itemName);
        $stmt->bindParam('itemType', $itemType);
        $stmt->bindParam('itemPrice', $itemPrice);
        $stmt->bindParam(':itemCat', $itemCat);
        $stmt->bindParam(':itemQty', $itemQty);
        $stmt->bindParam(':itemDesc', $itemDesc);
        $stmt->bindParam(':itemID', $itemID);
        $stmt->execute();
    }
    
    function delete_item($itemID){
        global $db;
        $stmt = $db->prepare('DELETE FROM product WHERE itemID = :itemID');
        $stmt->bindParam(':itemID', $itemID);
        $stmt->execute();
        return $stmt;
    }
    
    
?>