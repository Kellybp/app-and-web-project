<?php

    function get_User($userName, $password){
         global $db;
         $stmt = $db ->prepare('SELECT * FROM user WHERE userName = :userName AND password = :password');
         $stmt->bindParam(':userName', $userName);
         $stmt->bindParam(':password', $password);
         $stmt-> execute();
         return $stmt;
    }
    
     function register_User($userName, $password){
        global $db;
        $stmt = $db->prepare('INSERT INTO user(userName, password) VALUES (:userName, :password)');
        $stmt->bindParam(':userName', $userName);
        $stmt->bindParam(':password', $password);
        $stmt->execute();
    }


?>