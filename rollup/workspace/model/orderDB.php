<?php
    function getCartDetails($userID){
         global $db;
         $stmt = $db ->prepare('SELECT * FROM tbl_orderDetails WHERE userID = :userID AND orderID IS null');
         $stmt->bindParam(':userID', $userID);
         $stmt-> execute();
         return $stmt;
    }
    
    function getOrderDetails($userID,$orderID){
         global $db;
         $stmt = $db ->prepare('SELECT * FROM tbl_orderDetails WHERE userID = :userID AND orderID = :orderID');
         $stmt->bindParam(':userID', $userID);
         $stmt->bindParam(':orderID', $orderID);
         $stmt-> execute();
         return $stmt;
    }
    
    function addOrderDetailID($userID, $itemID){
        global $db;
        $stmt = $db-> prepare('INSERT INTO tbl_orderDetails(userID, itemID) VALUES (:userID, :itemID)');
        $stmt->bindParam(':userID', $userID);
        $stmt->bindParam(':itemID', $itemID);
        $stmt-> execute();
    }
    
    function placeOrder($shippingAddress, $userID){
        global $db;
        //making the record filed in order first
        $stmt = $db-> prepare('INSERT INTO tbl_order(shippingAddress) VALUES (:shippingAddress)');
        $stmt-> bindParam(':shippingAddress', $shippingAddress);
        $stmt-> execute();
        //finished making the order Record
        
       
        //selecting the orderID to add it to the Order Details table by the last id craeted
        $order = $db-> prepare('SELECT * FROM tbl_order ORDER BY orderID DESC LIMIT 1');
        $order-> execute();
        $order = $order->fetch();
        $orderID = $order['orderID'];
        //ending of selecting the order
        
        
        //updating the the order details table with the current orderID
        $stmt2 = $db->prepare('UPDATE tbl_orderDetails SET orderID = :orderID WHERE userID = :userID');
        $stmt2-> bindParam(':orderID', $orderID);
        $stmt2-> bindParam(':userID', $userID);
        
        $stmt2-> execute();
        //ending of placing the order
    }
    
    function getOrdersForDelivery(){
        global $db;
        $stmt = $db-> prepare('SELECT * from tbl_order');
        $stmt-> execute();
        return $stmt;
    }
    
    function delete_cart_item($cartID){
        global $db;
        $stmt = $db->prepare('DELETE FROM tbl_orderDetails WHERE orderDetailID = :orderDetailID');
        $stmt->bindParam(':orderDetailID', $cartID);
        $stmt->execute();
        return $stmt;
    }
?>