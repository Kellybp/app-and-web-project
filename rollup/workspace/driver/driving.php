<?php include '../views/viewheader.php'; ?>


  <h1>Divers map</h1>

      <div id="map"></div>
      
      <div class="row">
        <div class="col m6">
          <select id="start">
            <option value="14 Street Union Square"> My location</option>
            </select>
        </div>
        
        <div class="col m6">
          <select id="end">
            <option value="<?php echo $address; ?>"> <?php echo $address; ?> </option>
            </select>
        </div>
      </div>
      
      <div class="row">
        <div class="col s12 m4 l4">
          <a class="btn red" onclick="Materialize.toast('Help I am in trouble', 4000)">Panic</a>
        </div>
        <div class="col s12 m4 l4">
          <a class="btn yellow" onclick="Materialize.toast('I need a refill', 4000)">Refill</a>
        </div>
        <div class="col s12 m4 l4">
        <a class="btn green" onclick="Materialize.toast('This order is done', 4000)">Order finished</a>
        </div
        
      </div>
          
          
          
          <div id="warnings-panel"></div>
    <script>
      function initMap(){
         var markerArray = [];

        // Instantiate a directions service.
        var directionsService = new google.maps.DirectionsService;

        // Create a map and center it on Manhattan.
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 13,
          center: {lat: 40.771, lng: -73.974}
        });

        // Create a renderer for directions and bind it to the map.
        var directionsDisplay = new google.maps.DirectionsRenderer({map: map});

        // Instantiate an info window to hold step text.
        var stepDisplay = new google.maps.InfoWindow;

        // Display the route between the initial start and end selections.
        calculateAndDisplayRoute(
            directionsDisplay, directionsService, markerArray, stepDisplay, map);
        // Listen to change events from the start and end lists.
        var onChangeHandler = function() {
          calculateAndDisplayRoute(
              directionsDisplay, directionsService, markerArray, stepDisplay, map);
        };
        document.getElementById('start').addEventListener('change', onChangeHandler);
        document.getElementById('end').addEventListener('change', onChangeHandler);
      }

      function calculateAndDisplayRoute(directionsDisplay, directionsService,
          markerArray, stepDisplay, map) {
        // First, remove any existing markers from the map.
        for (var i = 0; i < markerArray.length; i++) {
          markerArray[i].setMap(null);
        }

        // Retrieve the start and end locations and create a DirectionsRequest using
        // WALKING directions.
        directionsService.route({
          origin: document.getElementById('start').value,
          destination: document.getElementById('end').value,
          travelMode: 'DRIVING'
        }, function(response, status) {
          // Route the directions and pass the response to a function to create
          // markers for each step.
          if (status === 'OK') {
            document.getElementById('warnings-panel').innerHTML =
                '<b>' + response.routes[0].warnings + '</b>';
            directionsDisplay.setDirections(response);
            showSteps(response, markerArray, stepDisplay, map);
          } else {
            window.alert('Directions request failed due to ' + status);
          }
        });
      }
    </script>
    
    
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcejNVX9GwfcAfmT4Sf2cpmUGWavl5Jt4&callback=initMap">
      
    </script>
<?php include '../views/viewfooter.php'; ?>