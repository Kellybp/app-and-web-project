<?php include '../views/viewheader.php'; ?>


<div class="row">
    <h3 class="center">Add an Item</h3>
    <form class="col s12" id="add_item_form" action="index.php" method="post">
      <input type="hidden" name="action" value="add_item"/>
      <div class="row">
        <div class="input-field col s12 l6">
          <input required="" aria-required="true"  placeholder="Enter Item Name" id="itemName" type="text" name="itemName" class="validate">
          <label for="itemName">Item Name</label>
        </div>
        <div class="input-field col s12 l6">
          <input required="" aria-required="true" placeholder="Enter Item Type" id="itemType" type="text" name="itemType" class="validate">
          <label for="itemType">Item Type</label>
        </div>
      </div>
      <div class="row">
          <div class="input-field col s12 l6">
            <input required="" aria-required="true" placeholder="Enter Price" id="itemPrice" type="text" name="itemPrice" class="validate">
            <label for="itemPrice">Price</label>
          </div>
          <div class="input-field col s12 l6">
            <input required="" aria-required="true" placeholder="Enter Quantity" id="itemQty" type="text" name="itemQty" class="validate">
            <label for="itemQty">Quantity</label>
          </div>
      </div>
      <div class="row">
        <div class="input-field col s12 l6">
            <input required="" aria-required="true" placeholder="Enter Item Category" id="itemCat" type="text" name="itemCat" class="validate">
            <label for="itemCat">Item Category</label>
          </div>
        <div class="file-field input-field col s12 l6">
          <div class="btn">
            <span>File</span>
            <input type="file" id="itemFile">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text" placeholder="Please Upload an Image of the Product">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12 l12">
          <textarea required="" aria-required="true" placeholder="Type Description Here" id="itemDesc" type="text" name="itemDesc" class="materialize-textarea validate"></textarea>  
          <label for="itemDesc">Item Description</label>
        </div>
      </div>

      <a class="btn waves-effect" href=".">Go Back</a>
      <button class="btn waves-effect" type="submit" name="Add Item">Submit Addition</button>
      
    </form>
  </div>



<?php include '../views/viewfooter.php'; ?>