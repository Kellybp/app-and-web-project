<?php include '../views/viewheader.php'; ?>


<div class="row">
  <h3 class="center">Edit an Item</h3>
    <form class="col s12" id="edit_item_form" action="index.php" method="post">
         <input type="hidden" name="action" value="edit_item" />
        <?php foreach($items as $item): ?>
      <input type="hidden" name="itemID" value="<?php echo $item['itemID']; ?>">
      <div class="row">
        <div class="input-field col s6">
          <input required="" aria-required="true" value="<?php echo $item['itemName']; ?>" id="itemName" type="text" name="itemName" class="validate">
          <label for="itemName">Item Name</label>
        </div>
        <div class="input-field col s6">
          <input required="" aria-required="true" value="<?php echo $item['itemType']; ?>" id="itemType" type="text" name="itemType" class="validate">
          <label for="itemType">Item Type</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s6">
          <input required="" aria-required="true" value="<?php echo $item['itemPrice']; ?>" id="itemPrice" type="text" name="itemPrice" class="validate">
          <label for="itemPrice">Item Price</label>
        </div>
        <div class="input-field col s6">
          <input required="" aria-required="true" value="<?php echo $item['itemCat']; ?>" id="itemCat" type="text" name="itemCat" class="validate">
          <label for="itemCat">Item Category</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s6">
          <input required="" aria-required="true" value="<?php echo $item['itemQty']; ?>" id="itemQty" type="text" name="itemQty" class="validate">
          <label for="itemQty">Quantity</label>
        </div>
        <div class="file-field input-field col s12 l6">
          <div class="btn">
            <span>File</span>
            <input type="file" id="itemFile">
          </div>
          <div class="file-path-wrapper">
            <input class="file-path validate" type="text" placeholder="Please Upload an Image of the Product">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12 l12">
          <textarea required="" aria-required="true" id="itemDesc" type="text" name="itemDesc" class="materialize-textarea validate"><?php echo $item['itemDesc']; ?></textarea>  
          <label for="itemDesc">Item Description</label>
        </div>
      </div>
      <?php endforeach; ?>
      <a class="btn waves-effect" href=".">Go Back</a>
      <button class="btn waves-effect" type="submit" name="Edit Item">Submit Changes</button>
      
    </form>
  </div>
<p>**To make a change, a field cannot be empty**</p>

<?php include '../views/viewfooter.php'; ?>