<?php include '../views/viewheader.php';?>
<h3 class="center">Store/Item Manager</h3>
<p>**In order to view an image and/or description, please view the item's page in the store, administrators can quickly make edits to image and/or description previews quickly there!**</p>
     <table class="striped">
        <thead>
          <tr>
              <th>Item Name</th>
              <th>Item Price</th>
              <th>Item Quantity</th>
              <th>Item Category</th>
              <th>Item Actions</th>
          </tr>
        </thead>

        <tbody>
            <?php foreach($storeItems as $Item): ?>
          <tr>
            <td><?php echo $Item['itemName']; ?></td>
            <td><?php echo $Item['itemPrice']; ?></td>
            <td><?php echo $Item['itemQty']; ?></td>
            <td><?php echo $Item['itemCat']; ?></td>
            <td>
                <form action="." method="post">
                    <input type="hidden" name="action" value="delete_item" />
                    <input type="hidden" name="itemID" value="<?php echo $Item['itemID'] ?>" />
                    <a class="btn waves-effect" href="?action=edit_item_form&itemID=<?php echo $Item['itemID'];?>"> Edit </a>
                    <button class="btn waves-effect" type="submit" name="Delete Item"> Delete </button>
                </form>
            </td>
          </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
      <br/>
      <a class="btn waves-effect" href="?action=add_item_form">Add Item</a>
      <br>
      <br>
      


<?php include '../views/viewfooter.php';?>