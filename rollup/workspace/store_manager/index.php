<?php
require('../model/database.php');
require('../model/itemDB.php');

    if(isset($_POST['action'])){
        
        $action = $_POST['action'];
        
    }else if(isset($_GET['action'])){
        
        $action = $_GET['action'];
    } else {
        
        $action = 'store_items';
        
    }
    
    if($action == 'store_items'){
        //making a lists of items in a table
        
        $storeItems = get_items();
        include('list_items.php');
        
    }else if($action == 'add_item_form'){
        
        include('add_item_form.php');
        
    }else if($action == 'add_item'){
        
        $itemName = $_POST['itemName'];
        $itemType= $_POST['itemType'];
        $itemPrice = $_POST['itemPrice'];
        $itemCat = $_POST['itemCat'];
        $itemQty = $_POST['itemQty'];
        $itemDesc = $_POST['itemDesc'];
        
        add_items($itemName, $itemType, $itemPrice, $itemCat, $itemQty, $itemDesc);
        
         $storeItems = get_items();
         include('list_items.php');
         
    }else if($action == 'edit_item_form'){
        
        $itemID = $_GET['itemID'];
        
        $items = get_item_by_ID($itemID);
        
        include('edit_item_form.php');
        
    } else if($action == 'edit_item'){
        
        $itemName = $_POST['itemName'];
        $itemType = $_POST['itemType'];
        $itemPrice = $_POST['itemPrice'];
        $itemCat = $_POST['itemCat'];
        $itemQty = $_POST['itemQty'];
        $itemDesc = $_POST['itemDesc'];
        $itemID = $_POST['itemID'];
        
        update_items($itemName, $itemType, $itemPrice, $itemCat, $itemQty, $itemDesc, $itemID);
        
        $storeItems = get_items();
        include('list_items.php');
    }else if($action == 'delete_item'){
        
        $itemID = $_POST['itemID'];
        delete_item($itemID);
        
        $storeItems = get_items();
        include('list_items.php');
    }


?>