<?php
require('../model/database.php');
require('../model/itemDB.php');
require('../model/orderDB.php');

    if(isset($_POST['action'])){
        
        $action = $_POST['action'];
        
    }else if(isset($_GET['action'])){
        
        $action = $_GET['action'];
    } else {
        
        $action = 'store_items';
        
    }
    
    if($action == 'store_items'){
        
        $Buds = get_items_buds();
        $Oils = get_items_oils();
        $Edibles = get_items_edibles();
        include('store_items.php');
        
    }else if($action == 'add_item'){
        
        $itemID = $_GET['itemID'];
        $userID = 1;
        
        addOrderDetailID($userID, $itemID);
        $Buds = get_items_buds();
        $Oils = get_items_oils();
        $Edibles = get_items_edibles();
        include('store_items.php');
        
    } elseif ($action == 'show_item') {
        $itemID = $_GET['itemID'];
        
        $items = get_item_by_ID($itemID);
        include('show_item.php');
    } else if($action == 'edit_item_form'){
        
        $itemID = $_GET['itemID'];
        
        $items = get_item_by_ID($itemID);
        
        include('../store_manager/edit_item_form.php');
        
    }


?>