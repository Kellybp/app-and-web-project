<?php include '../views/viewheader.php'; ?>
<br/>
    
    <div class="row">
        <div class="col s12">
            <ul class="tabs">
                <!-- Static tabbed layout -->
                <li class="tab col s4"><a class="active" href="#buds">Buds</a></li>
                <li class="tab col s4"><a href="#edibles">Edibles</a></li>
                <li class="tab col s4"><a href="#oils">Oils</a></li>
            </ul>
        </div>
        <!-- Same repurposed foreach loop from earlier, though now there will be an if statement distinguishing between which item categories -->
        
                <div id="buds">
                    <?php foreach($Buds as $Item): ?>
                    <div class="col s12 m6 l4"><!--Col size goes here-->
                    <div class="card">
                        <div class="card-image">
                            <!--
                                <img src="https://resize.mantisadnetwork.com/mantis-ad-network/image/fetch/f_jpg,q_95/https://uploads.medicaljane.com/wp-content/uploads/2013/01/ogkushFULL2.jpg">
                            -->
                            <img src="../images/buds.jpg">
                            <span id="white_title" class="card-title"><?php echo $Item['itemName']; ?></span>
                        </div>
                        <div class="card-content">
                            <p><?php echo $Item['itemQty']; ?></p>
                            <p><?php echo $Item['itemPrice']; ?></p>
                        </div>
                        <div class="card-action">
                            <div class="text-truncate"><p><?php echo $Item['itemDesc']; ?></p></div>
                            <br>
                            <a class="btn waves-effect" href="?action=show_item&itemID=<?php echo $Item['itemID']; ?>">See More</a>
                            <br>
                            <br>
                            <a href="?action=add_item&itemID=<?php echo $Item['itemID']; ?>"> Add To Cart </a>
                        </div>
                    </div>
                    </div>
                    <?php endforeach; ?>
                </div>

                <div id="edibles">
                    <?php foreach($Edibles as $Item): ?>
                    <div class="col s12 m6 l4"><!--Col size goes here-->
                    <div class="card">
                        <div class="card-image">
                            <img src="../images/edibles.jpg">
                            <span id="white_title" class="card-title"><?php echo $Item['itemName']; ?></span>
                        </div>
                        <div class="card-content">
                            <p><?php echo $Item['itemQty']; ?></p>
                            <p><?php echo $Item['itemPrice']; ?></p>
                        </div>
                        <div class="card-action">
                            <span class="truncate"><?php echo $Item['itemDesc']; ?></span>
                            <br>
                            <a class="btn waves-effect" href="?action=show_item&itemID=<?php echo $Item['itemID']; ?>">See More</a>
                            <br>
                            <br>
                            <a href="?action=add_item&itemID=<?php echo $Item['itemID']; ?>"> Add To Cart </a>
                        </div>
                    </div>
                    </div>
                    <?php endforeach; ?>
                </div>


                <div id="oils">
                    <?php foreach($Oils as $Item): ?>
                    <div class="col s12 m6 l4"><!--Col size goes here-->
                    <div class="card">
                        <div class="card-image">
                            <img src="../images/oils.jpg">
                            <span id="white_title" class="card-title"><?php echo $Item['itemName']; ?></span>
                        </div>
                        <div class="card-content">
                            <p><?php echo $Item['itemQty']; ?></p>
                            <p><?php echo $Item['itemPrice']; ?></p>
                        </div>
                        <div class="card-action">
                            <span class="truncate"><?php echo $Item['itemDesc']; ?></span>
                            <br>
                            <a class="btn waves-effect" href="?action=show_item&itemID=<?php echo $Item['itemID']; ?>">See More</a>
                            <br>
                            <br>
                            <a href="?action=add_item&itemID=<?php echo $Item['itemID']; ?>"> Add To Cart </a>
                        </div>
                    </div>
                    </div>
                  <?php endforeach; ?>
                </div>
    </div>

<?php include '../views/viewfooter.php'; ?>