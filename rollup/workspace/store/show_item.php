<?php include '../views/viewheader.php'; ?>


<?php foreach($items as $Item): ?>
<h3 class="center"><?php echo $Item['itemName']; ?></h3>
<div class="row">
    <div class="col s12 m4 l4">
        <?php
            if ($Item['itemCat'] == 'Buds'){
                echo '
                <img class="responsive-img materialboxed" src="../images/buds.jpg">
                ';
            } elseif ($Item['itemCat'] == 'Edibles'){
                echo '
                <img class="responsive-img materialboxed" src="../images/edibles.jpg">
                ';
            } elseif ($Item['itemCat'] == 'Oils'){
                echo '
                <img class="responsive-img materialboxed" src="../images/oils.jpg">
                ';
            } else {
            }
        ?>
    </div>
    <div class="col s12 m8 l8">
        <div class="row">
            <div class="col s12">
                <h5>Description:</h5>
                <br>
                <span><?php echo $Item['itemDesc']; ?></span>
                <br>
                <br>
                <br>
            </div>
            <div class="col s6">
                <h5>Product Details:</h5>
                <ul>
                    <li><strong>Quantity:</strong> <?php echo $Item['itemQty']; ?></li>
                    <li><strong>Price:</strong> <?php echo $Item['itemPrice']; ?></li>
                    <!--<li><strong>Item Category:</strong><//?php echo $Item['itemCat']; ?></li>-->
                    <!--<li><strong>Quality:</strong> <//?php echo $Item['itemType']; ?></li>-->
                </ul>
            </div>
            <div class="col s6">
                <a class="btn waves-effect" href=".">Go Back</a>
                <br>
                <br>
                <a class="btn waves-effect" href="?action=edit_item_form&itemID=<?php echo $Item["itemID"];?>">Edit Item</a>
                <br>
                <br>
                <p>Add Quantity: <a class="waves-effect waves-teal btn-flat"><i class="material-icons">remove</i></a> 1 <a class="waves-effect waves-teal btn-flat"><i class="material-icons">add</i></a></p>
                <a class="btn waves-effect" href="?action=add_item&itemID=<?php echo $Item["itemID"]; ?>"> Add To Cart </a>
                <?php
                /*
                // This is the switch statement that restricts navigation between the different user roles, customer, administrator, driver, or not logged in
                $userRole = "admin";
                // "customer" or :"driver"
                switch ($userRole) {
                  case "admin":
                    echo '
                        <a class="btn waves-effect" href=".">Go Back</a>
                        <br>
                        <br>
                        <a class="btn waves-effect" href="?action=edit_item_form&itemID=<?php echo $Item["itemID"];?>"> Edit </a>
                        <br>
                        <br>
                        <a class="btn waves-effect" href="?action=add_item&itemID=<?php echo $Item["itemID"]; ?>"> Add To Cart </a>
                          ';
                  break;
                  case "driver":
                    echo 'YOU SHOULD NOT BE LOOKING TO TO GET LIITT FAM ON YOUR WORK ACCOUNT!!!!';
                  break;
                  case "customer":
                    echo '
                        <a class="btn waves-effect" href=".">Go Back</a>
                        <br>
                        <br>
                        <a class="btn waves-effect" href="?action=add_item&itemID=<?php echo $Item["itemID"]; ?>"> Add To Cart </a>
                          ';
                  break;
                  //if not designated a user role, by default it is presumed that they are not logged in
                  default:
                    echo 'You Should NOT BE SEEING ANY OF THIS SINCE YOU ARE NOT LOGGED IN!!!';
                }
                */
                ?>
                
            </div>
        </div>
        
    </div>
</div>
<?php endforeach; ?>


<?php include '../views/viewfooter.php'; ?>