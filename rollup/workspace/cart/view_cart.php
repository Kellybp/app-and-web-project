<?php include '../views/viewheader.php'; ?>

    <h3 class="center">Check Out</h3>
    <table class="responsive-table centered">
      <thead>
        <tr>
            <th>Item CartID <!--Should Just be an incremental number for however many items are in the cart--> </th>
            <th>Picture of Product</th>
            <th>Product ID <!--data bind product name instead of showing product id--></th>
            <th>Item Quantity</th>
            <th>Remove this Item?</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($cartItems as $Item): ?>
        <tr>
            <td><?php echo $Item['orderDetailID']; ?></td>
            <td class="center"><img class="materialboxed center-align" id="cartImg" data-caption="Name of the Product Goes Here" width="100" src="../images/buds.jpg"></td>
            <td><?php echo $Item['itemID']; ?></td>
            <td><a class="waves-effect waves-teal btn-flat"><i class="material-icons">remove</i></a> {Quantity} <a class="waves-effect waves-teal btn-flat"><i class="material-icons">add</i></a></td>
            <td>
              <form action="." method="post">  
                <input type="hidden" name="action" value="delete_cart_item" />
                <input type="hidden" name="cartID" value="<?php echo $Item['orderDetailID'] ?>" />
                <button class="btn waves-effect" type="submit" name="Delete Item">Remove</button>
              </form>
            </td>
        </tr>
        <?php endforeach;?>
      </tbody>
    </table>
    <br>
    <br>
    <br>
    <form class="col s12" id="check_out_form" action="index.php" method="post">
      <input type="hidden" name="action" value="checkout"/>
      <div class="row">
        <div class="col s6">
          <h5 class="center">Ship To:</h5>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s6">
          <input required="" aria-required="true" placeholder="Address Line 1" id="shipping1" type="text" name="shippingAddress" class="validate">
          <label for="shipping1">Shipping Address Line 1</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s6">
          <input placeholder="Address Line 2" id="shipping2" type="text" name="shipping2" class="validate">
          <label for="shipping2">Shipping Address Line 2</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s6">
          <input required="" aria-required="true" placeholder="City" id="city" type="text" name="shippingAddress" class="validate">
          <label for="city">City</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s6">
          <input required="" aria-required="true" placeholder="State" id="state" type="text" name="shippingAddress" class="validate">
          <label for="state">State</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s6">
          <input required="" aria-required="true" placeholder="Zip Code" id="zip" type="text" name="shippingAddress" class="validate">
          <label for="zip">Zip Code</label>
        </div>
      </div>
      <button class="btn waves-effect" type="submit" name="Checkout">Check out</button>
      <br>
      <br>
    </form>
    
    
<?php include '../views/viewfooter.php'; ?>