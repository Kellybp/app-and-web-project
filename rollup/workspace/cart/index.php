<?php
require('../model/database.php');
require('../model/itemDB.php');
require('../model/orderDB.php');


 if(isset($_POST['action'])){
        
        $action = $_POST['action'];
        
    }else if(isset($_GET['action'])){
        
        $action = $_GET['action'];
    } else {
        
        $action = 'view_cart';
    }

    if($action == 'view_cart'){
        
        $userID = 1;
        $cartItems = getCartDetails($userID);
        
        include('view_cart.php');
    }else if($action == 'checkout'){
        
        $shippingAddress = $_POST['shippingAddress'];
        $userID = 1;
        
        
        placeOrder($shippingAddress, $userID);
        include('thankyou.php');
    } else if($action == 'delete_cart_item'){
        
        $cartID = $_POST['orderDetailID'];
        delete_item($cartID);
        
        $userID = 1;
        $cartItems = getCartDetails($userID);
        
        include('view_cart.php');
    }


?>