<?php include 'views/header.php'; ?>
    
    <!-- This is where the image will go-->
    <div class="row">
        <!--
        <div id="background">
        </div>
        -->
        
        <!--
        <div class="carousel carousel-slider">
            <a class="carousel-item" href="#one!">
                <img src="images/back1.jpg">
                <h5 class="left-align" style="vertical-align:bottom>This should be left aligned</h5>
            </a>
            <a class="carousel-item" href="#two!">
                <img src="images/back1.jpg">
                <h5 class="center-align" style="vertical-align:bottom">This should be centrally aligned</h5>
            </a>
            <a class="carousel-item" href="#three!">
                <img src="images/back1.jpg">
                <h5 class="right-align" style="vertical-align:bottom">This should be right aligned</h5>
            </a>
        </div>
        -->
        
            <div class="slider">
                <ul class="slides">
                  <li>
                    <img src="images/back1.jpg">
                    <div class="caption center-align">
                    <br>
                    <br>
                      <h3 class="black-text">Welcome to Rollup!</h3>
                      <h5 class="black-text">The <strong><u>only</u></strong> medical marajuana delivery servce</h5>
                    </div>
                  </li>
                  <li>
                    <img src="images/back1.jpg">
                    <div class="caption left-align">
                    <br>
                    <br>
                      <h3 class="black-text">On sale this week: Grandaddy Purple</h3>
                      <h5 class="black-text">Buy 2 get 1 free <a id="sale-link" href="#">Click here</a> for more details</h5>
                    </div>
                  </li>
                  <li>
                    <img src="images/back1.jpg">
                    <div class="caption right-align">
                    <br>
                    <br>
                      <h3 class="black-text">This month: 15% off any vape accessory</h3>
                      <h5 class="black-text"><a id="sale-link" href="#">Click here</a> to browse vape accessories</h5>
                    </div>
                  </li>
                  </ul>
            </div>
        </div>
    
    <!--This is the 3 colum text-->
    <div class="row">
        <div class="col m4">
            <div class="icon-block">
                <h2 class="center">
                    <i class="material-icons">flash_on</i>
                </h2>
                <h4 class="center-align">Quick</h4>
                <p class="center-align">We have drivers working at all hours to get you the best product as fast as possible, any time day or night</p>
            </div>
        </div>
        <div class="col m4">
            <div class="icon-block">
                <h2 class="center">
                    <i class="material-icons">beenhere</i>
                </h2>
            <h4 class="center-align">Safe</h4>
            <p class="center-align">We verify all Drivers with full background checks and a comprehensive screening process</p>
            </div>
        </div>
        <div class="col m4">
            <div class="icon-block">
                <h2 class="center">
                    <i class="material-icons">group</i>
                </h2>
            <h4 class="center-align">Professional</h4>
            <p class="center-align">We abide by all state and federal medical marijuana laws</p>
            </div>
        </div>
    </div>
    
    <!--This is the disclaimer-->
    <div class="row blue">
        <h1 class="center-align">Disclaimer this is a prototype</h1>
    </div>

<?php include 'views/footer.php'; ?>