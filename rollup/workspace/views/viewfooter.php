  </div>
  </main>
  <footer style="background-color: #4c921a;" class="primary-green">
    <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="white-text">Rollup</h5>
                <p class="grey-text text-lighten-4">The only medical marijuana delivery service</p>
                <p class="grey-text text-lighten-4"><small>Disclaimer *Users must be 21 or older, be able to provide adequate documentation proving eligibility to purchase products. Users forging information are subject to prosecution under the fullest extent of the law*</small></p>
              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Follow Us!</h5>
                <ul>
                  <li><a class="grey-text text-lighten-3" href="#!"><i class="fa fa-facebook-official" style="font-size:24px" aria-hidden="true"></i> @Rollup</a></a></li>
                  <li><a class="grey-text text-lighten-3" href="#!"><i class="fa fa-twitter-square" style="font-size:24px" aria-hidden="true"></i> @Rollup</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!"><i class="fa fa-instagram" style="font-size:24px" aria-hidden="true"></i> @RollupOfficial</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!"><i class="fa fa-snapchat-square" style="font-size:24px" aria-hidden="true"></i> @Rollup</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
              <div class="grey-text text-lighten-4" style="padding-bottom: .75em;">
            &copy; 2017 Copyright Rollup
            </div>
            </div>
          </div>
        </footer>   
        
        
        
        <!--Import jQuery before materialize.js-->
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script type="text/javascript" src="../js/materialize.js"></script>
        <script type="text/javascript" src="../js/init.js"></script>
    </body>
</html>