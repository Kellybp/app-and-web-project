<!DOCTYPE html>
<html>
    <head>
        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link type="text/css" rel="stylesheet" href="../css/materialize.css"  media="screen,projection"/>
        
        <link rel="stylesheet" href="../css/style.css" type="text/css" />
        
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">

        <link rel="icon" type="image/png" href="../images/favicon.png" />
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        
        <title>Roll Up Services</title>
    </head>

    <body>
        <nav style="background-color: #4c921a;" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="../" class="brand-logo valign-wrapper" style="font-family: 'Lobster', cursive;"><img id="logoImg" class="responsive-img circle" src="../images/favicon.png" /> Roll Up</a>
      <ul class="right hide-on-med-and-down">
        <?php
        // This is the switch statement that restricts navigation between the different user roles, customer, administrator, driver, or not logged in
        $userRole = "admin";
        // "customer" or :"driver"
        switch ($userRole) {
          case "admin":
            echo '<li><a href="../">Home</a></li>
                  <li><a href="../store">Store</a></li>
                  <li><a href="../store_manager">Store Manager</a></li>
                  <li><a href="../user/show_user.php">Users Online</a></li>
                  <li><a href="../cart">Cart</a></li>
                  <li><a href="../driver">Driver</a></li>';
          break;
          case "driver":
            echo '<li><a href="../">Home</a></li>
                  <li><a href="../driver">Driver</a></li>';
          break;
          case "customer":
            echo '<li><a href="../">Home</a></li>
                  <li><a href="../store">Store</a></li>
                  <li><a href="../cart">Cart</a></li>';
          break;
          //if not designated a user role, by default it is presumed that they are not logged in
          default:
            echo '<li><a href="../">Home</a></li>
                  <li><a href="../user">Login</a></li>
                  <li><a href="../user/register">Register</a></li>';
        }
        ?>
        <!--
        
        --Old default html navigation that showed everything to everybody
        
        <li><a href="../">Home</a></li>
        <li><a href="../user">Login</a></li>
        <li><a href="../user/register">Register</a></li>
        <li><a href="../store">Store</a></li>
        <li><a href="../store_manager">Store Manager</a></li>
        <li><a href="../driver">Driver</a></li>
        <li><a href="../cart">Cart</a></li>
        -->
      </ul>

      <ul id="nav-mobile" class="side-nav">
        <?php
        // This is the switch statement that restricts navigation between the different user roles, customer, administrator, driver, or not logged in
        $userRole = "admin";
        // "customer" or :"driver"
        switch ($userRole) {
          case "admin":
            echo '<li><a href="../">Home</a></li>
                  <li><a href="../store">Store</a></li>
                  <li><a href="../store_manager">Store Manager</a></li>
                  <li><a href="../user/show_user.php">Users Online</a></li>
                  <li><a href="../cart">Cart</a></li>
                  <li><a href="../driver">Driver</a></li>';
          break;
          case "driver":
            echo '<li><a href="../">Home</a></li>
                  <li><a href="../driver">Driver</a></li>';
          break;
          case "customer":
            echo '<li><a href="../">Home</a></li>
                  <li><a href="../store">Store</a></li>
                  <li><a href="../cart">Cart</a></li>';
          break;
          //if not designated a user role, by default it is presumed that they are not logged in
          default:
            echo '<li><a href="../">Home</a></li>
                  <li><a href="../user">Login</a></li>
                  <li><a href="../user/register">Register</a></li>';
        }
        ?>
        <!--
        
        --Old default html navigation that showed everything to everybody
        
        <li><a href="../">Home</a></li>
        <li><a href="../user">Login</a></li>
        <li><a href="../user/register">Register</a></li>
        <li><a href="../store">Store</a></li>
        <li><a href="../store_manager">Store Manager</a></li>
        <li><a href="../driver">Driver</a></li>
        <li><a href="../cart">Cart</a></li>
        -->
      </ul>
      <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
    </div>
  </nav>
  <main>
    <div class="container">